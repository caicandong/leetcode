package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var s string
	fmt.Scanln(&s)
	numS := strings.Split(s, ",")
	nums := make([]int, len(numS))
	for i := 0; i < len(numS); i++ {
		nums[i], _ = strconv.Atoi(numS[i])
	}
	sort.Ints(nums)
	for i := 0; i < len(nums); i++ {
		fmt.Printf("%d,", nums[i])
	}
}
