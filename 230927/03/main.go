package main

import (
	"fmt"
	"strconv"
	"strings"
)

func helper(ip1, ip2 string) int {
	// 全部转为32位二进制
	nums1 := strings.Split(ip1, ".")
	nums2 := strings.Split(ip2, ".")
	ret := 0
	for i := 0; i < 4; i++ {
		if nums1[i] == nums2[i] {
			ret += 8
			continue
		}
		a, _ := strconv.Atoi(nums1[i])
		b, _ := strconv.Atoi(nums2[i])
		mask := 128
		for a&mask == b&mask {
			ret += 1
			mask >>= 1
			// fmt.Println(mask)
		}
		break
	}
	return ret
}
func main() {
	var ip1, ip2 string
	fmt.Scanf("%s %s", &ip1, &ip2)
	fmt.Println("Prefix length:", helper(ip1, ip2))
}
