# 最长不重复子数组长度
from collections import Counter

nums = list(map(int, input().strip().split(' ')))
# nums = [
ret = 0
i,j = 0,0
cnt = Counter()
while i<len(nums):
    cnt[nums[i]]+=1
    while j<i and cnt[nums[i]]>1:
        cnt[nums[j]]-=1
        j+=1
    ret = max(ret, i-j+1)
    i+=1
print(ret)