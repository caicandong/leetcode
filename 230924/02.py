nums = list(map(int, input()[1:-1].split(",")))
target = int(input())


def solution(nums, target):
    # 贪心算法
    nums = [x for x in nums if x<=target]
    nums.sort(reverse=True)
    ret = target+1
    # print(ret)
    def dfs(i,cnt,target):
        if cnt>100:
            return
        nonlocal ret
        if cnt >= ret: # 剪枝
            return
        if i>=len(nums): # 越界
            return
        if target%nums[i]==0: # 可以凑出
            ret = min(ret, cnt+target//nums[i])
            return
        # 贪心, 从大到小
        # for x in range(target//nums[i],-1,-1):
        x = target//nums[i]
        dfs(i+1, cnt+x, target-x*nums[i])
        dfs(i+1, cnt, target)

    dfs(0,0,target)

    if ret == target+1:
        print(-1)
    else:
        print(ret)

solution(nums, target)