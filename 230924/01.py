# 3 7 2 => 12
# 1 7   => 8


# 1 7 2 => 10
# 3 7   => 10

nums1 = list(map(int, input().split()))
nums2 = list(map(int, input().split()))
# nums1 = [3,7,2]
# nums2= [1,7]

diff = sum(nums1)-sum(nums2)
if diff ==0:
    print("fail")
    exit()
for i,j in zip(nums1, nums2):
    if i-j==diff//2:
        print(i,j)
        exit()

print("fail")