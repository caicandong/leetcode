# 所有可能的出栈顺序
# 递归

# abc
def solution(nums):
    # print(len(nums))
    def dfs(stack,i, path):
        # print(stack,i, path)
        if len(path)==len(nums):
            print(path)
            return
        if stack:
            # print(path+stack[-1])
            dfs(stack[:-1],i, path+stack[-1])
        if i>=len(nums):
            return
        stack.append(nums[i])
        # print(stack,path)
        dfs(stack[:],i+1, path)
    dfs([],0,"")
s = input()
solution(s)
