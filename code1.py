"""
3
...
o*o
...
o**
ooo
..*.
*o*
*o*
o*o
    """
def solution(nums):
    memory = {"*":0,"o":0}
    for i in range(3): # 左右被吃
        if nums[i][1] == "*" and nums[i][2] == nums[i][0] == "o":
            memory["*"]+=1
        if nums[i][1] == "o" and nums[i][2] == nums[i][0] == "*":
            memory["o"]+=1
    for j in range(3): # 上下被吃
        if nums[1][j] == "*" and nums[2][j] == nums[0][j] == "o":
            memory["*"]+=1
        if nums[1][j] == "o" and nums[2][j] == nums[0][j] == "*":
            memory["o"]+=1
    # print(memory)
    if memory["*"]>0 and memory["o"]>0:
        return "draw"
    if memory["*"]>0 and memory["o"]==0:
        return "yukari"
    if memory["*"]==0 and memory["o"]>0:
        return "kou"
    # 0 0
    return "draw"
def main():
    n = int(input())
    for i in range(n):
        nums = []
        for j in range(3):
            nums.append(input().strip())
        print(solution(nums))
main()
    # nums1 = ["...",
    #     "o*o",
    #     "..." ]
    # nums2 = [
    #     "o**",
    #     "ooo",
    #     "..*"
    # ]
    # nums3 = [
    #     "*o*",
    #     "*o*",
    #     "o*o"
    # ]
    # print(solution(nums3))
main()