import copy

def solution(nums):
    length = len(nums)
    results = [-1] * length
    min_heap = copy.deepcopy(nums)

    for i in range(length):
        min_heap.sort() # used for heapifying min heap
        first_min = min_heap[0] # stores first minimum value of heap

        if first_min != nums[i] or len(min_heap) == 1:
            results[i] = first_min
            del min_heap[0] # removing first minimum
        else:
            second_min = min_heap[1] # stores second minimum value of heap
            results[i] = second_min
            del min_heap[1] # removing second minimum

    if results[length - 1] == nums[length - 1] and length >= 2:
        temp = results[length - 2]
        results[length - 2] = results[length - 1]
        results[length - 1] = temp
    return results

nums = [1, 2, 3,5,6,4,8,7,9]
print(*solution(nums))