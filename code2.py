def helper(nums,pos,neg):
    ret = []
    for x,y,z in zip(nums,pos,neg):
        ret.append(max(0,x-y+z))
    return ret,sum(ret)

def main():
    n = 4 # 1<=n<=20
    nums = [0,1,0,1]
    m = 3
    medicine = [
        [[1,1,0,0],[0,0,1,0]],
        [[0,1,0,1],[1,0,0,0]],
        [[1,0,0,1],[0,0,0,0]]
        ]
    q=3    # 1<=q<=10000
    queries = [2,3,1]
    ret = []
    for i in queries: # O(n*4)
        nums,x= helper(nums,medicine[i-1][0],medicine[i-1][1]) # O(n*2)
        ret.append(x)
    print(ret)

main()


    # 最开始小红有第二个、第四个症状。
    # 开始小红服用了第二副药，治好了这两个症状，但新增了第一个症状。 => 1
    # 目前症状数量为1然后小红服用了第三副药，治好了第一个症状，并没有新增症状。目前症状数量为0。 =>0
    # 最后小红服用了第一副药。由于小红没有症状，所以没有治疗，但又新增了第三个症状，目前症状数量为1。=>1