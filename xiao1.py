"""
3
...
o*o
...
o**
ooo
..*.
*o*
*o*
o*o
    """

# b d q p
def solution(s):
    i,j = 0,len(s)-1
    s = list(s)
    while i<j:
        if s[i]==s[j]: # 相等
            i+=1
            j-=1
        elif s[i] in "bdqp" and s[j] in "bdqp": # 轴对称&翻转
            i+=1
            j-=1
        elif s[i] in "nu" and s[j] in "nu":  # 轴对称&翻转
            i+=1
            j-=1
        elif s[i]=='m' and s[j] in "nu":
            s[i]='n'
            j-=1
        elif s[i] in "nu" and s[j]=='m':
            i+=1
            s[j]='n'
        elif s[i]=='w' and s[j] == 'v':
            s[i]='v'
            j-=1
        elif s[i]=='v' and s[j] == 'w':
            s[j]='v'
            i+=1
        else:
            return "NO"
    return "YES"

def main():
    n = int(input().strip())
    for i in range(n):
        s = input().strip()
        print(solution(s))
main()

"""
5
wovv
bod
pdd
moom
lalalai

1
pdd
"""
