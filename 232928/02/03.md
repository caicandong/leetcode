``` diff
func sumOfDistancesInTree(n int, edges [][]int) []int {
    g := make([][]int, n) // g[x] 表示 x 的所有邻居
    for _, e := range edges {
        x, y := e[0], e[1]
        g[x] = append(g[x], y)
        g[y] = append(g[y], x)
    }

    ans := make([]int, n)
    size := make([]int, n)
-   var dfs func(int, int, int)
-   dfs = func(x, fa, depth int) {
+   var dfs func(int, int)
+   dfs = func(x, depth int) {
        ans[0] += depth // depth 为 0 到 x 的距离
        size[x] = 1
        for _, y := range g[x] { // 遍历 x 的邻居 y
-         if y != fa { // 避免访问父节点
-               dfs(y, x, depth+1) // x 是 y 的父节点
+         if size[y] == 0 { // 避免访问父节点
+               dfs(y, depth+1)
                size[x] += size[y] // 累加 x 的儿子 y 的子树大小
            }
        }
    }
- dfs(0, -1, 0) // 0 没有父节点
+ dfs(0, 0) // 0 没有父节点

-   var reroot func(int, int)
-   reroot = func(x, fa int) {
+   var reroot func(int)
+   reroot = func(x int) {
        for _, y := range g[x] { // 遍历 x 的邻居 y
-           if y != fa { // 避免访问父节点
+           if ans[y] == 0 { // 避免访问父节点
                ans[y] = ans[x] + n - 2*size[y]
-               reroot(y, x) // x 是 y 的父节点
+               reroot(y) // x 是 y 的父节点
            }
        }
    }
-   reroot(0, -1) // 0 没有父节点
+   reroot(0)
    return ans
}
```