def helper(n,edges):
    grid = [[] for _ in range(n)]
    for x,y in edges:
        grid[x].append(y)
        grid[y].append(x)
    ans = [0]*n
    size = [1]*n
    def dfs(x,fa,depth):
        ans[0] += depth
        for y in grid[x]:
            if y!=fa:
                dfs(y,x,depth+1)
                size[x] += size[y]
    dfs(0,-1,0)
    def de(x,fa):
        for y in grid[x]:
            if y!=fa:
                ans[y] = ans[x] + n - 2*size[y]
                de(y,x)
    de(0,-1)
    return ans

n,m = list(map(int, input().split()))
nums = list(map(int, input().split()))
querys = list(map(int, input().split()))

nums = [num-1 for num in nums] # 0-based
querys = [num-1 for num in querys] # 0-based
edges =[]

for i,num in enumerate(nums):
    edges.append((i+1,num))

ans = helper(n,edges)
ret = [ans [i] for i in querys]
print(*ret)
"""
3 3
1 1
1 2 3
"""


