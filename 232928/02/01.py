# 5 3 4 5
from functools import cache
def check(nums):
    # 检查是否能构成三角形
    a,b,c = sorted(nums)
    if a+b>c:
        return True
    return False

def solution(n,a,b,c):
    memory = set()
    @cache
    def dfs(a,b,c,idx):
        nums = [a,b,c]
        nonlocal memory
        if check(nums):
            # print(*nums)
            memory.add(tuple(nums))
        if max(nums)<idx or idx>n:
            return
        for i in range(3):
            nums[i] -= idx
            if nums[i]>0:
                a,b,c = nums
                dfs(a,b,c,idx+1)
            nums[i]+=idx
    dfs(a,b,c,0)
    # print(memory)
    print(len(memory))

n,a,b,c = list(map(int, input().split()))
# solution(5,3,4,5)
solution(n,a,b,c)
# 最多加工n次
# 3 4 5  => 3 4 4 =>1 4 4

# 1,4,4 
# 2,2,2
# 10