#
# 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
#
#
# @param books int整型二维数组
# @return int整型
#
class Solution:
    def max_consistent_book_size(self , books ):
        # write code here
        # [
        #   [20210425, 20210501], b2
        #   [20210426, 20210504], b1
        #   [20210427, 20210504], b3
        #   [20210427, 20210502]  b4
        # ]
        # 最长递增子序列问题
        books = sorted(books, key=lambda x:x[0])
        nums = [x[1] for x in books]
        dp = [1]*len(nums)
        for i in range(1, len(nums)):
            for j in range(i):
                if nums[i]>=nums[j]:
                    dp[i] = max(dp[i], dp[j]+1)
        return max(dp)