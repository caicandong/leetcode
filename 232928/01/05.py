#
# 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
#
# 返回最大库区的分类数字
# @param grid int整型二维数组
# @return int整型
#
class Solution:
    def categoryOfMaxWarehouseArea(self , grid ):
        # write code here
        m ,n = len(grid), len(grid[0])
        direction = [(1,0),(-1,0),(0,1),(0,-1)] # 下 右 上 左
        def dfs(i,j):
            cur = grid[i][j]
            grid[i][j]=-1
            ret = 1
            for dx,dy in direction:
                x,y = i+dx,j+dy
                if 0<=x<m and 0<=y<n and grid[x][y]==cur:
                    ret += dfs(x,y)
            return ret
        result,cnt = grid[0][0],1
        for i in range(m):
            for j in range(n):
                if grid[i][j]==-1:
                    continue
                cur_result,cur_cnt = grid[i][j],dfs(i,j)
                # print(cur_result,cur_cnt)
                if cur_cnt>cnt:
                    result,cnt = cur_result,cur_cnt
        return result
print(Solution().categoryOfMaxWarehouseArea([[1, 1, 1, 2], [2, 1, 2, 2], [3, 2, 3, 3]]))