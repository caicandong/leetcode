class Solution:
    def numberOfShelves(self , N ):
        results = [[-1]*N for _ in range(N)]
        # write code here 逆时针方向旋转
        direction = [(1,0),(-1,1),(0,-1)] # 下 右 上 左
        x,y = 0,0
        cur_dir = 0
        i = 0
        while i <(N+1)*N//2:
            print(x,y)
            results[x][y] = i+1
            i+=1
            if i == (N+1)*N//2:
                break
            dx,dy = direction[cur_dir]
            while not (0<=x+dx<N and 0<=y+dy<N and x+dx+y+dy < N and results[x+dx][y+dy]==-1):
                cur_dir = (cur_dir+1)%3
                dx,dy = direction[cur_dir]
            x,y = x+dx,y+dy
        # print(results)
        ret = []
        for i in range(N):
            for j in range(N):
                if results[i][j]!=-1:
                    ret.append(results[i][j])
        return ret

print(Solution().numberOfShelves(5))

# [ [1, -1, -1],
#   [2, -1, 6],
#   [3, 4, 5]
# ]