#
# 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
#
#
# @param nums int整型一维数组 当前有货书籍的价格数组
# @param budget int整型 买书预算
# @return int整型
#
from collections import Counter
class Solution:
    def purchase(self , nums , budget ):
        # write code here
        cnt = Counter(nums)
        prices = sorted(cnt.keys())
        ret  = 0
        for i,x in enumerate(prices):
            for y in prices[i+1:]:
                if x+y < budget:
                    if x==y:
                        ret+= cnt[x]*(cnt[x]-1)//2
                    else:
                        ret += cnt[x]*cnt[y]
        return ret%(10**9+7)