# ((a)2(b)2)2
# (
# ((
# ((a
# ((a
# "(","aa"
# "(","aa","(","b"
class Solution:
    def decompress(self , compressed_str ):
        # write code here
        stack = []
        n  = len(compressed_str)
        i =0
        while i<n:
            if compressed_str[i] == '(': # 填入符号
                stack.append(compressed_str[i])
                i+=1
            elif compressed_str[i]==')': # 系数
                j = i+1
                while j<n and compressed_str[j].isdigit():
                    j+=1
                weight = int(compressed_str[i+1:j]) if i+1<j else 1
                cur_s = ""
                while stack and stack[-1]!='(':
                    cur_s = stack.pop()+cur_s
                stack.pop() # pop '('
                stack.append(cur_s*weight)
                i = j
            else:
                stack.append(compressed_str[i])
                i+=1
            # print(stack)
        return ''.join(stack)

print(Solution().decompress("((a)2(b)2)2"))