package main

import (
	"fmt"
	"sync"
)

func worker(id int, tasks <-chan string, results chan<- error, wg *sync.WaitGroup) {
	defer wg.Done()
	for task := range tasks {
		if err := processTask(task); err != nil {
			results <- fmt.Errorf("任务 %s 处理失败: %v", task, err)
			return
		}
	}
}

func processTask(task string) error {
	if task == "task3" {
		return fmt.Errorf("任务失败")
	}
	fmt.Printf("任务 %s 处理成功\n", task)
	return nil
}

func main() {
	numWorkers := 3 // 最多N个并发处理
	tasks := []string{"task1", "task2", "task3", "task4", "task5"}

	tasksChan := make(chan string, len(tasks))
	resultsChan := make(chan error, len(tasks))
	var wg sync.WaitGroup

	// 启动goroutine来处理任务
	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go worker(i, tasksChan, resultsChan, &wg)
	}

	// 将任务发送到tasksChan
	for _, task := range tasks {
		tasksChan <- task
	}
	close(tasksChan)

	// 等待所有任务完成
	wg.Wait()

	// 检查错误结果
	close(resultsChan)
	for err := range resultsChan {
		fmt.Println(err)
	}
}
