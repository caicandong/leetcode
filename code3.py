def solution(nums):
    m,n = len(nums),len(nums[0])
    dp = [[m*n]*n for _ in range(m)]
    dp[0][0]= 0
    for i in range(m):
        for j in range(n):
            if nums[i][j]=="*" or dp[i][j]==m*n :
                continue
            k = 1
            while k+i<m: # 向下走
                if nums[k+i][j]=="*":
                    break
                dp[k+i][j] = min(dp[i][j]+1,dp[k+i][j])
                k+=1
            k=1
            while k+j<n: # 向右走
                if nums[i][k+j]=="*":
                    break
                dp[i][k+j] = min(dp[i][j]+1,dp[i][k+j])
                k+=1
            k=1
            while k+i<m and k+j<n: # 向右下走
                if nums[k+i][k+j]=="*":
                    break
                dp[k+i][k+j] = min(dp[i][j]+1,dp[k+i][k+j])
                k+=1
    # print(dp)
    return dp[-1][-1]

def main():
    case1 = ["...",
             ".**",
             ".*."]
    case2 = ["....",
             "**.*",
             "...."]
    print(solution(case2))
    # # [0, 1, 1
    # #  1,-1,-1
    #    1,-1,-1
    # ]
main()
    # # [0, 1, 1, 1
    # #  -1,-1,2,-1
    #    -1,-1,2, 2
    # ]