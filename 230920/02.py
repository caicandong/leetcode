# 随机输出 win 或者 lose
import random
# random.seed(0)
# n = list(map(int, input().split()))[0]
# for i in range(n):
#     print(random.choice(["win","lose"]))



class Node:
    def __init__(self, data):
        self.data = data
        self.children = []
        self.visited = False

    def add_child(self, obj):
        self.children.append(obj)

    def __repr__(self):
        return str(self.data)

def count(node):
    if not node.children:
        return 1
    else:
        return sum([count(i) for i in node.children]) + 1

def count_path(node,x):
    if node.data == x:
        return 1
    elif not node.children:
        return -1
    for i in node.children:
        ret = count_path(i,x)
        if ret != -1:
            return ret + 1
    return -1
n = list(map(int, input().split()))[0]
for i in range(n):
    n,x = list(map(int, input().split()))
    nodes = [Node(i) for i in range(n+1)]
    for j in range(n-1):
        a,b = list(map(int, input().split()))
        nodes[a].add_child(nodes[b])
    # print("win")
    # x下面有多少节点
    # cnt = count(nodes[x])-1
    # if cnt ==0: # x是叶子节点
    #     print("win")
    #     continue
    # # 根节点到x有多少个节点
    # cnt_path = count_path(nodes[1],x)
    # if (n-cnt_path)%2 == 0:
    #     print("win")
    # else:
    #     print("lose")
    # print("----------------------------")
    # print(cnt,cnt_path)
    # print(solution(n,nodes[1],x))



"""
1
5 3
1 2
1 3
2 4
2 5

5 2

1 2
1 3
2 4
2 5

win
lose



1
5 2
1 2
1 3
2 4
2 5

"""