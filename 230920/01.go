package main

import "fmt"

func main() {
	s := []int{1, 2}       //a=1,2 len=2 cap=2
	s = append(s, 4, 5, 6) //b=1,2,4,5,6 len=5,cap=5
	fmt.Printf("len=%d, cap=%d", len(s), cap(s))
	// 5,5
}
