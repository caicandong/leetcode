x,y = list(map(int, input().split()))

xx = []
yy = []

while x>0 or y>0:
    xx.append(x&1)
    yy.append(y&1)
    x >>= 1
    y >>= 1
if sum(xx) < sum(yy):
    print(-1)
    exit()
# 移动1
n = 0
finnal = []
i,j = len(xx)-1,len(yy)-1
while i>=0 and j>=0:
    if xx[i]==yy[j]:
        i -= 1
        j -= 1
    elif xx[i]==1 and yy[j]==0:
        finnal.append(f"- {2**(i)}")
        n+=1
        i -= 1
        j -= 1
    elif xx[i]==0 and yy[j]==1:
        # 移动一个1过来
        logs =[]
        k = i-1
        while k>=0 and xx[k]!=1:
            logs.append(f"+ {2**k}")
            k -= 1
        logs.append(f"+ {2**k}")
        xx[k] = 0
        for log in logs[::-1]:
            # print(log)
            finnal.append(log)
            n+=1
        i -= 1
        j -= 1
print(len(finnal))
for log in finnal:
    print(log)
# # 统计x和y的二进制表示中1的个数
# def count(x):
#     cnt = 0
#     while x:
#         cnt += x&1
#         x >>= 1
#     return cnt


# if count(x) < count(y):
#     print(-1)

