# 命令行输入 并转化int list
n,m,H = list(map(int, input().split())) # 朋友的个数，楼梯的个数，小红的身高 3 4 4
h =  list(map(int, input().split())) # 代表每个朋友的身高  3 5 7
p = list(map(int, input().split()))  # 代表每个朋友的位置  1 2 2
s = list(map(int, input().split()))  # 代表每个楼梯的高度  1 2 3 3

# expect = 1
H= max(s)+H
ret = 0
for i in range(n):
    h[i] += s[p[i]-1]
    if H>h[i]:
        ret += 1

print(ret)

"""
3 4 4
3 5 7
1 2 2
1 2 3 3
"""
